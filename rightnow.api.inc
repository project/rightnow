<?php


// Function definitions
function rightnow_urlsafe_encode(&$str){
	return(strtr(base64_encode($str), array('+' => '_', '/' => '~', '=' => '*')));
}

function rightnow_rlsafe_decode(&$str){
	return(base64_decode(strtr($str, array('_' => '+', '~' => '/', '*' => '='))));
}


function rightnow_pta_login_redirect($account, $li_data){

	$settings = rightnow_get_settings();
	
	// ----------------------------------------------------------
	
	$domain = $settings['domain'];
	$interface = $settings['interface'];
	$script_dir = $settings['script_dir'];
	$mysec_li_passwd = $settings['mysec_li_passwd'];
	$php_bin = $settings['php_bin'];
	
	if( empty($domain) || empty($interface) ){
		return;
	}
	
	// ----------------------------------------------------------

	$li_data += array(

		// sample text contact custom field (custom_fields.cf_id== 1)
		//'p_ccf_1' => $li_ccf_1,
		// sample menu contact custom field (custom_fields.cf_id == 3)
		//'p_ccf_3' => intval($li_ccf_3),
		
		// p_li_passwd must match the MYSEC_LI_PASSWD config setting 
		'p_li_passwd' => $mysec_li_passwd,
		'p_redirect' => $settings['p_redirect'],
		//'p_next_page' => $settings['p_next_page'],
		'p_next_page' => 'http://dev2.inukshukfoundation.org/user',
	);
	
	if(empty($settings['assign_pass'])){
		unset($li_data['p_passwd']);
	}else if($settings['assign_pass'] == 'custom'){
		$li_data['p_passwd'] = $settings['custom_pass'];
	}
	
	// lets other modules modify
	drupal_alter('rightnow_login_params', $li_data);
	
	// set up the $p_li variable
	while (list($key, $val) = each($li_data)){
		$p_li .= sprintf("%s%s=%s", $p_li ? '&' : '', $key, $val);
	}
	
	$p_li = rightnow_urlsafe_encode($p_li);
	$parms = '';
	
	// retain all the important query_string parameters passed in from
	//RNW (excluding the special cases and the li_* form parameters)
	/*
	while (list($key, $val) = each($HTTP_GET_VARS)) {
		if (($key != 'p_next_page') && ($key != 'p_li') && (substr($key, 0, 3) != 'li_')){
			$parms .= sprintf("&%s=%s", $key, urlencode($val));
		}
	}
	*/

	// default next page to support home
	if (!isset($p_next_page)){
		$p_next_page = $settings['p_next_page'];
	}
	
	//$url = implode("/" , array($domain, $script_dir, $interface . '.cfg', $php_bin, 'enduser', $p_next_page));
	
	//$redirect = 'http://dev2.inukshukfoundation.org/user';
	//$parms .= sprintf("&%s=%s", 'p_next_page', rightnow_urlsafe_encode($redirect));
	
	// redirect to RN
	// drupal_goto not working???
	header("Content-type: text/html; charset=UTF-8");
	// not working :( if we follow may'10 style
	//header('Location: ' . $url . '?p_li=' . $p_li . $parms);
	header('Location: ' . $domain . '/ci/pta/login/redirect/' . $p_next_page . '/p_li/' . $p_li . $parms);
	exit();
}

/*http://estudies.custhelp.com/cgi-bin/estudies.cfg/php/enduser/home?p_li=cF91c2VyaWQ9YWRtaW4mcF9wYXNzd2Q9RHJ1cGFsMzIxJnBfZW1haWwuYWRkcj1wLmtAaW51a3NodWtmb3VuZGF0aW9uLm9yZyZwX25hbWUuZmlyc3Q9YWRtaW4mcF9uYW1lLmxhc3Q9YWRtaW4mcF9saV9wYXNzd2Q9JnBfcmVkaXJlY3Q9MSZwX25leHRfcGFnZT1ob21lLnBocA**
*/

function rightnow_pta_logout_redirect(){

	global $user;
	
	$settings = rightnow_get_settings();
	
	$domain = $settings['domain'];
	$interface = $settings['interface'];
	
	if( empty($domain) ){
		return;
	}
	
	drupal_goto("$domain/ci/pta/logout");
	
}

function right_now_go(){
	global $user;
	$account = $user;
	
	$q = $_GET['q'];
	
	$fname = $account->name;
	$lname = $account->name;
	
	$names = explode(' ', $account->name, 2);
	
	if(count($names) == 2){
		$fname = $names[0];
		$lname = $names[1];
	}
	
	$li_data = array(
		'p_userid' => $account->name,
		'p_passwd' => $edit['pass'],
		'p_email.addr' => $account->mail,
		'p_name.first' => $fname,
		'p_name.last' => $lname,
	);
	
	rightnow_pta_login_redirect($user, $li_data);
	
}