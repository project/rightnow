<?php

/**
 * @file
 * rightnow settings form.
 */
 

/**
 * rightnow settings form.
 */
function rightnow_admin_settings_form(&$form_state, $type) {
  
  $settings = rightnow_get_settings();
  
  
  $form['rightnow'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settigns'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => 0,
  );
	
	$form['rightnow']['domain'] = array(
		'#type' => 'textfield',
		'#title' => t('Domain'),
		'#default_value' => $settings['domain'],
		'#description' => t('Your RightNow domain name. example: http://example.custhelp.com'),
		'#required' => TRUE,
  );
	
  $form['rightnow']['interface'] = array(
		'#type' => 'textfield',
		'#title' => t('Interface'),
		'#default_value' => $settings['interface'],
		'#description' => t('Your RightNow interface name.'),
		'#required' => TRUE,
  );
	
  $form['rightnow']['php_bin'] = array(
		'#type' => 'select',
		'#title' => t('Php bin directory'),
		'#default_value' => $settings['php_bin'],
		'#options' => array('php' => t('php'), 'php.exe' => t('php.exe')),
		'#description' => t('php bin directory (php or php.exe). This is required to build the request URL.'),
		'#required' => TRUE,
  );
	
	$form['rightnow']['script_dir'] = array(
		'#type' => 'select',
		'#title' => t('script directory'),
		'#default_value' => $settings['script_dir'],
		'#options' => array('cgi-bin' => t('cgi-bin'), 'scripts' => t('scripts')),
		'#description' => t('script directory (cgi-bin or scripts). This is required to build the request URL.'),
		'#required' => TRUE,
  );
	
	$form['rightnow']['mysec_li_passwd'] = array(
		'#type' => 'textfield',
		'#title' => t('mysec_li_passwd'),
		'#default_value' => $settings['mysec_li_passwd'],
		'#description' => t('This parameter represents the string specified in the MYSEC_LI_PASSWD configuration setting. Note: This parameter is required if the MYSEC_LI_PASSWD configuration setting contains a value.'),
  );
	
	$form['rightnow']['mysec_li_passwd'] = array(
		'#type' => 'textfield',
		'#title' => t('MYSEC_LI_PASSWD'),
		'#default_value' => $settings['mysec_li_passwd'],
		'#description' => t('This parameter represents the string specified in the MYSEC_LI_PASSWD configuration setting. Note: This parameter is required if the MYSEC_LI_PASSWD configuration setting contains a value.'),
  );
	
	$form['rightnow']['p_next_page'] = array(
		'#type' => 'textfield',
		'#title' => t('Next page'),
		'#default_value' => $settings['p_next_page'],
		'#description' => t('Next page parameter. '),
  ); 
	
	$form['pass'] = array(
    '#type' => 'fieldset',
    '#title' => t('Password settigns'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => 1,
  );
	
	$form['pass']['assign_pass'] = array(
		'#type' => 'radios',
		'#title' => t('Assign password'),
		'#default_value' => $settings['assign_pass'],
		'#options' => array('' => ('NULL'), 'user' => t('User password (not recomented)'), 'custom' => t('Custom password')),
		'#description' => t(''),
		'#required' => TRUE,
  );
	
	$form['pass']['custom_pass'] = array(
		'#type' => 'textfield',
		'#title' => t('Custom password'),
		'#default_value' => $settings['custom_pass'],
		'#description' => t('Provide the custom password to assign to RightNow users. max limit 20.'),
		'#maxlength' => 20,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#weight' => 10,
  );
  
  return $form;
}

/**
 * rightnow settings form validate.
 */
function rightnow_admin_settings_form_validate($form, &$form_state) {

}

/**
 * rightnow settings submit.
 */
function rightnow_admin_settings_form_submit($form, &$form_state) {

  $settings = rightnow_get_settings();
	
  foreach (rightnow_settings_info() as $setting => $value) {
		//print($setting . ' ' . $form_state['values'][$setting]);
		$settings[$setting] = $form_state['values'][$setting];
  }
	//print_r($settings);
	
	variable_set('rightnow', $settings);
  drupal_set_message('Your changes have been saved.');

}


